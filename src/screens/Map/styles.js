import styled from 'styled-components';
import pageBackground from '../../assets/images/pageBackground.png';

export const Container = styled.div`
  background-image: url(${pageBackground});
  width: 100%;
  height: 100vh;
  background-position: center center;
`;

export const BoxCharacter = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
`;

export const BoxTooltip = styled.div`
  position: relative;

  :hover {
    > div {
      visibility: visible;
      @-webkit-keyframes animatebottom {
        from {
          top: 0px;
          opacity: 0;
        }
        to {
          top: -70px;
          opacity: 1;
        }
      }
      @keyframes animatebottom {
        from {
          top: 0px;
          opacity: 0;
        }
        to {
          top: -70px;
          opacity: 1;
        }
      }

      -webkit-animation-name: animatebottom;
      -webkit-animation-duration: 2s;
      animation-name: animatebottom;
      animation-duration: 2s;
    }
  }
`;

export const BoxBodyTooltip = styled.div`
  visibility: hidden;
  position: absolute;
  top: -70px;
  
  > img {
    cursor: pointer;
  }
`;

export const BoxTooltipFixed = styled.div`
  position: absolute;
  top: -70px;

  > img {
    cursor: pointer;
  }
`;
