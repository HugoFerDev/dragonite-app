import * as PokemonAction from '../../store/modules/pokemon/actions';
import * as MeAction from '../../store/modules/me/actions';

import React, { useEffect, useState } from 'react';
import Sidebar from '../../components/Sidebar';
import Modal from '../../components/Modal';
import {
  BoxBodyTooltip,
  BoxCharacter,
  BoxTooltip,
  BoxTooltipFixed,
  Container,
} from './styles';
import { useDispatch, useSelector } from 'react-redux';
import generateRandomNumber from '../../utils/generateRandomNumber';
import DetailPokemon from '../../components/DetailPokemon';
import { QUANTITY_POKEMON_SIDEBAR } from '../../constants';

const MapPage = () => {
  const dispatch = useDispatch();

  const [modal, setModal] = useState({ open: false });
  const [loading, setLoading] = useState(false);

  const pokemon = useSelector((state) => state.pokemon);
  const me = useSelector((state) => state.me);

  const getPokemon = () => {
    setLoading(true);
    setTimeout(() => {
      dispatch(PokemonAction.getPokemon(generateRandomNumber()));
    }, 2000);
  };

  const closeModal = () => {
    setModal({ open: false });
  };

  const capturePokemon = (item) => {
    dispatch(MeAction.capturePokemon(item));
    closeModal();
  }

  useEffect(() => {
    if (!pokemon.loading && pokemon.success) {
      setLoading(false);
      setModal({ open: true, item: pokemon.item });
    }
  }, [pokemon.loading]);

  const canCapturePokemon = me.pokemons.length < QUANTITY_POKEMON_SIDEBAR;

  return (
    <>
      <Container>
        <Sidebar pokemons={me.pokemons}/>

        <BoxCharacter>
          <div>
            {loading ? (
              <div>
                <BoxTooltipFixed>
                  <img
                    alt='Tooltip'
                    onClick={getPokemon}
                    src={require('../../assets/images/searchingTooltip.png')}
                  />
                </BoxTooltipFixed>
                <img
                  alt='Character Run'
                  src={require('../../assets/images/ashRun.gif')}
                />
              </div>
            ) : canCapturePokemon ? (
              <BoxTooltip>
                <BoxBodyTooltip>
                  <img
                    alt='Tooltip'
                    onClick={getPokemon}
                    src={require('../../assets/images/searchTooltip.png')}
                  />
                </BoxBodyTooltip>
                <img
                  onClick={getPokemon}
                  alt='Personagem'
                  src={require('../../assets/images/ashFront.png')}
                />
              </BoxTooltip>
            ) : (
              <BoxTooltip>
                <BoxBodyTooltip>
                  <img
                    alt='Tooltip'
                    src={require('../../assets/images/tooltipError.png')}
                  />
                </BoxBodyTooltip>
                <img
                  alt='Personagem'
                  src={require('../../assets/images/ashFront.png')}
                />
              </BoxTooltip>
            )}
          </div>
        </BoxCharacter>
        {modal.open ? (
          <Modal onClose={closeModal}>
            <DetailPokemon capturePokemon={capturePokemon} pokemon={pokemon.item} />
          </Modal>
        ) : null}
      </Container>
    </>
  );
};

export default MapPage;
