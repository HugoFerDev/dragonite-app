import styled from 'styled-components';

export const Container = styled.div`
  background: rgb(66,232,123);
  background: linear-gradient(111deg, rgba(66,232,123,1) 0%, rgba(56,248,213,1) 100%);
  width: 100%;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const BoxLogo = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  > button {
    margin-top: 2em;
  }
`