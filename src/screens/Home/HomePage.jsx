import React from 'react';
import Button from '../../components/Button';
import history from '../../services/history';
import { BoxLogo, Container } from './styles';

const HomePage = () => (
  <Container>
    <BoxLogo>
      <img alt='logo' src={require('../../assets/images/pokemonLogo.png')} />
      <Button onClick={() => history.push('/map')} text='Start' />
    </BoxLogo>
  </Container>
);

export default HomePage;
