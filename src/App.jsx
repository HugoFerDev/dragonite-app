import React from 'react';
import { Provider } from 'react-redux';
import { Route, Router, Switch } from 'react-router-dom';
import HomePage from './screens/Home/HomePage';
import MapPage from './screens/Map/MapPage';
import history from './services/history';
import store from './store';

const App = () => (
  <Provider store={store}>
    <Router history={history}>
      <>
        <Switch>
          <Route exact path='/' component={HomePage} />
          <Route exact path='/map' component={MapPage} />
          {/* <Route path="*" component={HomePage} /> */}
        </Switch>
      </>
    </Router>
  </Provider>
);

export default App;
