import React from 'react';
import chevron from '../assets/images/chevronDownBlack.png';
import { Flex } from './Flex';

const NumberInput = ({
  className,
  iconLabel,
  label,
  placeholder,
  name,
  suffix,
  onChange,
  error
}) => {
  return (
    <div className={`${className} input__container`}>
      {label && (
        <label className='input__label'>
          <Flex align='center'>
            {iconLabel ? iconLabel : null} {label}
          </Flex>
        </label>
      )}
      <div className='input__number'>
        <input
          className={`input ${error ? '__error' : ''}`}
          type='number'
          onChange={onChange}
          placeholder={placeholder}
          name={name}
        />
        {suffix && <p className='input__suffix'>{suffix}</p>}
        <div className='input__btns'>
          <img src={chevron} className='input__increase' alt='Mais' />
          <img src={chevron} className='input__decrease' alt='Menos' />
        </div>
      </div>
      {error ? <span className='input__message--error'>{error}</span> : null}
    </div>
  );
};

export default NumberInput;
