import styled from 'styled-components';

export const Flex = styled.div`
  display: flex;
  margin: ${props => props.margin || 0};
  justify-content: ${props => props.justify || 'flex-start'};
  align-items: ${props => props.align || 'flex-start'}
`