import React from 'react';
import { useTranslation } from 'react-i18next';
import Button from '../Button';
import { Flex } from '../Flex';
import {
  ContainerDetail,
  BoxImgCharacter,
  BoxDetail,
  TitleCharacter,
  BoxInfo,
  BoxTags,
  TitleSection,
  TagType,
  BoxAbility,
  FieldStatistic,
  BoxPokeball,
  BoxFreedom,
} from './styles';

const DetailPokemon = ({ pokemon, capturePokemon, toFreePokemon, isMy }) => {
  const { t } = useTranslation();

  const hp = pokemon.stats.find((e) => e.stat.name === 'hp');
  const attack = pokemon.stats.find((e) => e.stat.name === 'attack');
  const defense = pokemon.stats.find((e) => e.stat.name === 'defense');
  const specialAttack = pokemon.stats.find(
    (e) => e.stat.name === 'special-attack'
  );
  const specialDefense = pokemon.stats.find(
    (e) => e.stat.name === 'special-defense'
  );
  const speed = pokemon.stats.find((e) => e.stat.name === 'speed');

  return (
    <>
      <ContainerDetail>
        <BoxImgCharacter>
          <img
            alt='Character'
            src={pokemon.sprites.other['official-artwork'].front_default}
          />
        </BoxImgCharacter>
      </ContainerDetail>

      <BoxDetail>
        <TitleCharacter>{pokemon.species.name}</TitleCharacter>

        <BoxInfo>
          <div>
            <span>{t('common.hp')}</span>
            <p>
              {hp.effort}/{hp.base_stat}
            </p>
          </div>
          <div>
            <span>{t('common.height')}</span>
            <p>{(pokemon.height / 10).toFixed(2)} m</p>
          </div>
          <div>
            <span>{t('common.weight')}</span>
            <p>{(pokemon.weight / 10).toFixed(2)} Kg</p>
          </div>
        </BoxInfo>

        <div>
          <TitleSection>
            <span>{t('common.type')}</span>
          </TitleSection>

          <BoxTags>
            {pokemon.types.map((e, i) => (
              <TagType key={i} className={`type--${e.type.name}`}>
                {t(`type.${e.type.name}`)}
              </TagType>
            ))}
          </BoxTags>
        </div>

        <div>
          <TitleSection>
            <span>{t('common.skills')}</span>
          </TitleSection>

          <BoxAbility>
            {pokemon.abilities.map((e) => e.ability.name).join(', ')}
          </BoxAbility>
        </div>

        <div>
          <TitleSection>
            <span>{t('common.statistics')}</span>
          </TitleSection>

          <Flex margin='1rem 0' justify='space-between'>
            <Flex align='center'>
              <img
                alt='defesa'
                src={require('../../assets/images/shield.png')}
              />
              <FieldStatistic style={{ paddingLeft: '.5rem' }}>
                {t('common.defense')}
              </FieldStatistic>
            </Flex>
            <FieldStatistic>{defense.base_stat}</FieldStatistic>
          </Flex>

          <Flex margin='1rem 0' justify='space-between'>
            <Flex align='center'>
              <img
                alt='defesa'
                src={require('../../assets/images/sword.png')}
              />
              <FieldStatistic style={{ paddingLeft: '.5rem' }}>
                {t('common.attack')}
              </FieldStatistic>
            </Flex>
            <FieldStatistic>{attack.base_stat}</FieldStatistic>
          </Flex>

          <Flex margin='1rem 0' justify='space-between'>
            <Flex align='center'>
              <img
                alt='defesa'
                src={require('../../assets/images/shield.png')}
              />
              <FieldStatistic style={{ paddingLeft: '.5rem' }}>
                {t('common.special_defense')}
              </FieldStatistic>
            </Flex>
            <FieldStatistic>{specialDefense.base_stat}</FieldStatistic>
          </Flex>

          <Flex margin='1rem 0' justify='space-between'>
            <Flex align='center'>
              <img
                alt='defesa'
                src={require('../../assets/images/sword.png')}
              />
              <FieldStatistic style={{ paddingLeft: '.5rem' }}>
                {t('common.special_attack')}
              </FieldStatistic>
            </Flex>
            <FieldStatistic>{specialAttack.base_stat}</FieldStatistic>
          </Flex>

          <Flex margin='1rem 0' justify='space-between'>
            <Flex align='center'>
              <img
                alt='defesa'
                src={require('../../assets/images/speed.png')}
              />
              <FieldStatistic style={{ paddingLeft: '.5rem' }}>
                {t('common.speed')}
              </FieldStatistic>
            </Flex>
            <FieldStatistic>{speed.base_stat}</FieldStatistic>
          </Flex>
        </div>
      </BoxDetail>

      {isMy ? (
        <BoxFreedom>
          <Button
            onClick={() => toFreePokemon(pokemon)}
            text={t('common.to_free_pokemon')}
          />
        </BoxFreedom>
      ) : (
        <BoxPokeball>
          <img
            onClick={() => capturePokemon(pokemon)}
            alt='capture'
            src={require('../../assets/images/pokeball.png')}
          />
        </BoxPokeball>
      )}
    </>
  );
};

export default DetailPokemon;
