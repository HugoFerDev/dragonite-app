import * as MeActions from '../store/modules/me/actions';

import React, { useState } from 'react';
import Button from './Button';
import plusIcon from '../assets/images/plus.png';
import { QUANTITY_POKEMON_SIDEBAR } from '../constants';
import DetailPokemon from './DetailPokemon';
import Modal from './Modal';
import { useDispatch, useSelector } from 'react-redux';
import NewPokemon from './NewPokemon';

const Sidebar = ({ pokemons }) => {
  const [modal, setModal] = useState({ open: false, type: 'detail' });

  const dispatch = useDispatch();
  const options = useSelector(state => state.options);

  let toList = [...pokemons] || [];
  if (pokemons && pokemons.length < QUANTITY_POKEMON_SIDEBAR) {
    for (let i = pokemons.length; i < QUANTITY_POKEMON_SIDEBAR; i++) {
      toList.push({ default: true });
    }
  }

  const toFreePokemon = (pokemon) => {
    dispatch(MeActions.toFreePokemon(pokemon));
    closeModal();
  };

  const capturePokemon = (pokemon) => {
    dispatch(MeActions.capturePokemon(pokemon));
    closeModal();
  };

  const handleDetailPokemon = (item) => {
    setModal({ open: true, type: 'detail', item });
  };

  const handleCreatePokemon = () => {
    setModal({ open: true, type: 'new' });
  };

  const closeModal = () => {
    setModal({ open: false, type: 'detail' });
  };

  return (
    <div className='sidebar'>
      {toList.map((pokemon, i) => {
        if (pokemon.default) {
          return <div key={i} className='sidebar__item'>?</div>;
        }
        return (
          <div key={i} className='sidebar__item'>
            <img
              alt='item'
              onClick={() => handleDetailPokemon(pokemon)}
              src={pokemon.sprites.other['official-artwork'].front_default}
            />
          </div>
        );
      })}

      <Button onClick={handleCreatePokemon} icon={<img src={plusIcon} alt='+' />} />

      {modal.open ? (
        <Modal onClose={closeModal}>
          {modal.type === 'new' ? (
            <NewPokemon capturePokemon={capturePokemon} types={options.types} />
          ) : (
            <DetailPokemon
              toFreePokemon={toFreePokemon}
              pokemon={modal.item}
              isMy={true}
            />
          )}
        </Modal>
      ) : null}
    </div>
  );
};

export default Sidebar;
