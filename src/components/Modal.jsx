import React from 'react';
import PropTypes from 'prop-types';
import closeIcon from '../assets/images/close.png';

const Modal = ({ children, onClose }) => (
    <div className="modal">
        <div className="modal__content">
            <img className="modal__close" onClick={onClose} src={closeIcon} alt="Fechar" />
            {children}
        </div>
    </div>
);

Modal.propTypes = {
    children: PropTypes.node.isRequired,
};

export default Modal;
