import * as Yup from 'yup';
import { useFormik } from 'formik';
import React from 'react';
import generateRandomNumber from '../../utils/generateRandomNumber';
import Button from '../Button';
import Dropdown from '../Dropdown';
import NumberInput from '../NumberInput';
import plusIcon from '../../assets/images/plus.png';
import TextInput from '../TextInput';
import {
  ContainerDetail,
  BoxImgCharacter,
  BoxDetail,
  TitleSection,
  BoxFreedom,
  BoxCamera,
} from './styles';

const NewPokemon = ({ capturePokemon, types }) => {
  const formik = useFormik({
    validateOnChange: false,
    validateOnBlur: false,
    initialValues: {
      abilities: [
        {
          ability: {
            name: '',
          },
        },
        {
          ability: {
            name: '',
          },
        },
        {
          ability: {
            name: '',
          },
        },
        {
          ability: {
            name: '',
          },
        },
      ],
      sprites: {
        other: {
          'official-artwork': {
            front_default: '',
          },
        },
      },
      species: {
        name: '',
      },
      stats: [
        {
          stat: {
            name: 'hp',
          },
          effort: 0,
          base_stat: 0,
        },
        {
          stat: {
            name: 'attack',
          },
          effort: 0,
          base_stat: 0,
        },
        {
          stat: {
            name: 'defense',
          },
          effort: 0,
          base_stat: 0,
        },
        {
          stat: {
            name: 'special-attack',
          },
          effort: 0,
          base_stat: 0,
        },
        {
          stat: {
            name: 'special-defense',
          },
          effort: 0,
          base_stat: 0,
        },
        {
          stat: {
            name: 'speed',
          },
          effort: 0,
          base_stat: 0,
        },
      ],
      height: 0,
      weight: 0,
      types: [],
    },
    validationSchema: Yup.object().shape({
      abilities: Yup.array().of(
        Yup.object().shape({
          ability: Yup.object().shape({
            name: Yup.string().required('Campo Obrigatório'),
          }),
        })
      ),
      sprites: Yup.object().shape({
        other: Yup.object().shape({
          'official-artwork': Yup.string().required('Campo Obrigatório'),
        }),
      }),
      species: Yup.object().shape({
        name: Yup.string().required('Campo Obrigatório'),
      }),
      stats: Yup.array().of(
        Yup.object().shape({
          base_stat: Yup.number().min(1, 'Campo Obrigatório'),
        })
      ),
      height: Yup.number().min(1, 'Campo Obrigatório'),
      weight: Yup.number().min(1, 'Campo Obrigatório'),
      types: Yup.array()
        .min(1, 'Campo Obrigatório')
        .max(2, 'O Pokémon pode ter no máximo 2 tipos'),
    }),
    onSubmit: (values) => {
      let pokemon = { ...values };
      pokemon.weight = values.weight * 10;
      pokemon.height = (values.height / 100) * 10;
      pokemon.id = generateRandomNumber(1000, 99999);
      pokemon.types = pokemon.types.map((e) => ({ type: { name: e } }));

      capturePokemon(pokemon);
    },
  });

  const toBase64 = (file) =>
    new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });

  const handleImage = (event) => {
    const file = event.target.files[0];
    toBase64(file).then((base64) =>
      formik.setFieldValue(
        'sprites.other.official-artwork.front_default',
        base64
      )
    );
  };

  return (
    <>
      <ContainerDetail>
        <BoxImgCharacter>
          <BoxCamera>
            <img
              alt='Character'
              src={
                formik.values.sprites.other['official-artwork'].front_default
                  ? formik.values.sprites.other['official-artwork']
                      .front_default
                  : require('../../assets/images/camera.png')
              }
            />
            <Button
              icon={
                <img
                  onClick={() => document.getElementById('inputImage').click()}
                  src={plusIcon}
                  alt='+'
                />
              }
            />
            <input
              style={{ display: 'none' }}
              id='inputImage'
              onChange={handleImage}
              type='file'
              accept='image/x-png,image/gif,image/jpeg'
            />
          </BoxCamera>
        </BoxImgCharacter>
      </ContainerDetail>

      <BoxDetail>
        <TextInput
          onChange={formik.handleChange}
          name='species.name'
          error={
            formik.errors && formik.errors.species && formik.errors.species.name
          }
          className='__margin'
          label='Nome'
          placeholder='Nome'
        />

        <NumberInput
          onChange={formik.handleChange}
          name='stats[0].base_stat'
          error={
            formik.errors &&
            formik.errors.stats &&
            formik.errors.stats[0].base_stat
          }
          className='__margin'
          label='HP'
          placeholder='HP'
        />

        <NumberInput
          onChange={formik.handleChange}
          name='weight'
          error={formik.errors && formik.errors.weight}
          className='__margin'
          label='Peso'
          placeholder='Peso'
          suffix='Kg'
        />
        <NumberInput
          onChange={formik.handleChange}
          name='height'
          error={formik.errors && formik.errors.height}
          className='__margin'
          label='Altura'
          placeholder='Altura'
          suffix='Cm'
        />

        <div>
          <TitleSection>
            <span>Tipo</span>
          </TitleSection>

          <Dropdown
            error={formik.errors.types}
            onChange={formik.handleChange}
            name='types'
            multiple={true}
            options={types}
          />
        </div>

        <div>
          <TitleSection>
            <span>Habilidades</span>
          </TitleSection>

          <TextInput
            onChange={formik.handleChange}
            name='abilities[0].ability.name'
            error={
              formik.errors &&
              formik.errors.abilities &&
              formik.errors.abilities[0].ability.name
            }
            className='__margin'
            placeholder='Habilidade 1'
          />
          <TextInput
            onChange={formik.handleChange}
            name='abilities[1].ability.name'
            error={
              formik.errors &&
              formik.errors.abilities &&
              formik.errors.abilities[1].ability.name
            }
            className='__margin'
            placeholder='Habilidade 2'
          />
          <TextInput
            onChange={formik.handleChange}
            name='abilities[2].ability.name'
            error={
              formik.errors &&
              formik.errors.abilities &&
              formik.errors.abilities[2].ability.name
            }
            className='__margin'
            placeholder='Habilidade 3'
          />
          <TextInput
            onChange={formik.handleChange}
            name='abilities[3].ability.name'
            error={
              formik.errors &&
              formik.errors.abilities &&
              formik.errors.abilities[3].ability.name
            }
            className='__margin'
            placeholder='Habilidade 4'
          />
        </div>

        <div>
          <TitleSection>
            <span>Estatísticas</span>
          </TitleSection>

          <NumberInput
            onChange={formik.handleChange}
            name='stats[2].base_stat'
            error={
              formik.errors &&
              formik.errors.stats &&
              formik.errors.stats[2].base_stat
            }
            iconLabel={
              <img
                alt='Defesa'
                style={{ paddingRight: '0.5rem' }}
                src={require('../../assets/images/shield.png')}
              />
            }
            className='__margin'
            label='Defesa'
            placeholder='00'
          />

          <NumberInput
            onChange={formik.handleChange}
            name='stats[1].base_stat'
            error={
              formik.errors &&
              formik.errors.stats &&
              formik.errors.stats[1].base_stat
            }
            iconLabel={
              <img
                alt='Ataque'
                style={{ paddingRight: '0.5rem' }}
                src={require('../../assets/images/sword.png')}
              />
            }
            className='__margin'
            label='Ataque'
            placeholder='00'
          />

          <NumberInput
            onChange={formik.handleChange}
            name='stats[4].base_stat'
            error={
              formik.errors &&
              formik.errors.stats &&
              formik.errors.stats[4].base_stat
            }
            iconLabel={
              <img
                alt='Defesa Especial'
                style={{ paddingRight: '0.5rem' }}
                src={require('../../assets/images/shield.png')}
              />
            }
            className='__margin'
            label='Defesa'
            placeholder='00'
          />

          <NumberInput
            onChange={formik.handleChange}
            name='stats[3].base_stat'
            error={
              formik.errors &&
              formik.errors.stats &&
              formik.errors.stats[3].base_stat
            }
            iconLabel={
              <img
                alt='Ataque Especial'
                style={{ paddingRight: '0.5rem' }}
                src={require('../../assets/images/sword.png')}
              />
            }
            className='__margin'
            label='Ataque Especial'
            placeholder='00'
          />

          <NumberInput
            onChange={formik.handleChange}
            name='stats[5].base_stat'
            error={
              formik.errors &&
              formik.errors.stats &&
              formik.errors.stats[5].base_stat
            }
            iconLabel={
              <img
                alt='Velocidade'
                style={{ paddingRight: '0.5rem' }}
                src={require('../../assets/images/speed.png')}
              />
            }
            className='__margin'
            label='Velocidade'
            placeholder='00'
          />
        </div>
      </BoxDetail>

      <BoxFreedom>
        <Button onClick={formik.handleSubmit} text='Criar Pokemon' />
      </BoxFreedom>
    </>
  );
};

export default NewPokemon;
