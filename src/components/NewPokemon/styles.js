import styled from 'styled-components';

export const ContainerDetail = styled.div`
  height: 25rem;
  width: 30rem;
  background: rgb(66, 232, 123);
  background: linear-gradient(
    111deg,
    rgba(66, 232, 123, 1) 0%,
    rgba(56, 248, 213, 1) 100%
  );
  display: flex;
  justify-content: center;
  padding-top: 2rem;
`;

export const BoxImgCharacter = styled.div`
  border-radius: 50%;
  border: #00d68f solid 4px;
  background: #fff;
  width: 20em;
  height: 20em;
  z-index: 2;
  display: flex;
  justify-content: center;
  align-items: center;

  > img {
    width: 10rem;
  }
`;

export const BoxDetail = styled.div`
  background: #fff;
  position: relative;
  height: 25rem;
  border-top-right-radius: 24px;
  border-top-left-radius: 24px;
  padding-right: 20px;
  padding-left: 20px;
  padding-bottom: 10rem;
  height: 50rem;
  margin-top: -13rem;
  padding-top: 12rem;
  color: #2e3a59;
  overflow: auto;

  
  ::-webkit-scrollbar {
    width: 6px;
  }

  /* Track */
  ::-webkit-scrollbar-track {
    background: #f1f1f1;
  }

  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: #bbbbbb;
    border-radius: 12px;
  }

  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: #999999;
  }
`;

export const BoxCamera = styled.div`
  position: relative;

  > button {
    border: 4px solid #fff;
    width: 3.4rem;
    height: 3.4rem;
    padding: 0.6rem 1.4rem;
    position: absolute;
    top: 50px;
    right: -10px;
  }
`;

export const BoxInfo = styled.div`
  display: flex;
  justify-content: center;
  color: #2e3a59;
  font-weight: bold;
  justify-content: space-around;
  padding-top: 2rem;

  div {
    text-align: center;
    width: 33.33%;

    &:first-child,
    &:nth-child(2) {
      border-right: solid #c5cee0 1px;
    }
  }

  span {
    font-size: 12px;
  }

  p {
    font-size: 18px;
    padding-top: 1rem;
  }
`;

export const TitleSection = styled.h2`
  width: 100%;
  text-align: center;
  border-bottom: 1px solid #c5cee0;
  line-height: 0.1em;
  margin: 10px 0 20px;
  margin: 3rem 0;

  > span {
    background: #fff;
    padding: 0 10px;
    font-weight: 500;
  }
`;

export const BoxTags = styled.div`
  display: flex;
  justify-content: space-around;
`;

export const TagType = styled.div`
  min-width: 2rem;
  background: ${props => props.background};
  color: #fff;
  padding: 10px;
  font-weight: bold;
  text-transform: uppercase;
  border-bottom-right-radius: 15px;
  border-bottom-left-radius: 15px;
  border-top-right-radius: 15px;
  border-top-left-radius: 15px;
  justify-content: space-around;
`

export const BoxAbility = styled.div`
  text-align: center;
  font-size: 10rem;
  font-size: 1.2rem;
  font-weight: bold;
  text-transform: uppercase;
`

export const FieldStatistic = styled.span`
  font-weight: bold;
  font-size: 1.1rem;
  text-transform: uppercase;
`

export const BoxPokeball = styled.div`
  position: relative;
  top: -100px;
  background: transparent;
  left: 35%;
  height: 0px;

  > img {
    cursor: pointer;
  }
`

export const BoxFreedom = styled.div`
  position: relative;
  top: -100px;
  background: transparent;
  left: 18%;
  height: 0px;

  > button {
    padding: .8rem 2.4rem;
  }
`