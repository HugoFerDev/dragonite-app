import React from 'react';

const TextInput = ({
  className,
  label,
  placeholder,
  onChange,
  name,
  error,
}) => {
  return (
    <div className={`${className} input__container`}>
      {label && <label className='input__label'>{label}</label>}
      <input
        className={`input ${error ? '__error' : ''}`}
        onChange={onChange}
        type='text'
        placeholder={placeholder}
        name={name}
      />
      {error ? <span className='input__message--error'>{error}</span> : null}
    </div>
  );
};

export default TextInput;
