import React from 'react';
import chevron from '../assets/images/chevronDownBlack.png';

const Dropdown = ({ options, onChange, multiple, error, name }) => {
  return (
    <div className='dropdown__container'>
      <img src={chevron} className='dropdown__icon' alt='Chevron' />
      <select
        className={`dropdown ${error ? '__error' : ''}`}
        onChange={onChange}
        multiple={multiple}
        name={name}
      >
        <option className='dropdown__option' value=''>
          Selecione o(s) tipo(s)
        </option>
        {options &&
          options.map((option, index) => (
            <option
              key={index}
              className='dropdown__option'
              value={option.value}
            >
              {option.text}
            </option>
          ))}
      </select>

      {error ? <span className='dropdown__message--error'>{error}</span> : null}
    </div>
  );
};

export default Dropdown;
