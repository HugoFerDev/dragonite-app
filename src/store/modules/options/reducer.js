const InitialState = {
  types: [
    {
      value: 'bug',
      text: 'Erro',
    },
    {
      value: 'dark',
      text: 'Escuro',
    },
    {
      value: 'dragon',
      text: 'Dragão',
    },
    {
      value: 'eletric',
      text: 'Elétrico',
    },
    {
      value: 'fairy',
      text: 'Fada',
    },
    {
      value: 'fighting',
      text: 'Brigando',
    },
    {
      value: 'fire',
      text: 'Fogo',
    },
    {
      value: 'flying',
      text: 'Vôo',
    },
    {
      value: 'ghost',
      text: 'Fantasma',
    },
    {
      value: 'grass',
      text: 'Relva',
    },
    {
      value: 'ground',
      text: 'Terra',
    },
    {
      value: 'ice',
      text: 'Gelo',
    },
    {
      value: 'normal',
      text: 'Normal',
    },
    {
      value: 'poison',
      text: 'Poção',
    },
    {
      value: 'psychic',
      text: 'Psíquico',
    },
    {
      value: 'rock',
      text: 'Rock',
    },
    {
      value: 'steel',
      text: 'Aço',
    },
    {
      value: 'water',
      text: 'Água',
    },
  ],
};

export default function options(state = InitialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}
