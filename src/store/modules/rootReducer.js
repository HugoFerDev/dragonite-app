import pokemon from './pokemon/reducer';
import me from './me/reducer';
import options from './options/reducer';

import { combineReducers } from 'redux';

export default combineReducers({
  pokemon,
  me,
  options
});
