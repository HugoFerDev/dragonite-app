import { CAPTURE_POKEMON, TO_FREE_POKEMON } from './types';

export const capturePokemon = (pokemon) => (dispatch) => {
  dispatch({
    type: CAPTURE_POKEMON,
    response: pokemon,
  });
};

export const toFreePokemon = (pokemon) => (dispatch) => {
  dispatch({
    type: TO_FREE_POKEMON,
    response: pokemon,
  });
};
