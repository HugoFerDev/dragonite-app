import { CAPTURE_POKEMON, TO_FREE_POKEMON } from './types';

const InitialState = {
  loading: false,
  pokemons: [],
};

export default function me(state = InitialState, action) {
  switch (action.type) {
    case CAPTURE_POKEMON:
      return {
        ...state,
        loading: false,
        pokemons: [...state.pokemons, action.response],
      };

    case TO_FREE_POKEMON:
      let index = state.pokemons.findIndex((e) => e.id === action.response.id);
      let newPokemons = [...state.pokemons];

      newPokemons.splice(index, 1);

      return {
        ...state,
        pokemons: newPokemons,
      };

    default:
      return state;
  }
}
