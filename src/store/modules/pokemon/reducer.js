import {
  GET_POKEMON_FAILED,
  GET_POKEMON_REQUEST,
  GET_POKEMON_SUCCESS,
} from './types';

const InitialState = {
  loading: false,
  success: false,
  collection: [],
  item: {},
};

export default function pokemon(state = InitialState, action) {
  switch (action.type) {
    case GET_POKEMON_REQUEST:
      return {
        ...state,
        loading: true,
        success: true,
      };

    case GET_POKEMON_SUCCESS:
      return {
        ...state,
        loading: false,
        success: true,
        item: action.response,
      };

    case GET_POKEMON_FAILED:
      return {
        ...state,
        loading: false,
      };

    default:
      return state;
  }
}
