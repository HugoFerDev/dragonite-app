import { GET_POKEMON_FAILED, GET_POKEMON_REQUEST, GET_POKEMON_SUCCESS } from './types';

import api from '../../../services/api';

export const getPokemon = id => dispatch => {
  dispatch({
    type: GET_POKEMON_REQUEST,
  });

  api
    .get(`/pokemon/${id}`)
    .then(({ data }) => {
      dispatch({
        type: GET_POKEMON_SUCCESS,
        response: data,
      });
    })
    .catch(error => {
      dispatch({
        type: GET_POKEMON_FAILED,
      });
    });
};
