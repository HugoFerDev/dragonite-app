export default (min, limit) => {
  min = min === undefined ? 1 : min;
  limit = limit || 807;

  return Math.floor(Math.random() * limit) + min;
};
